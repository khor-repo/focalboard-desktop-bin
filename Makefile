default:
	@echo "Usage: make clean"

install:
	@makepkg -i
	
uninstall:
	@sudo pacman -Rs focalboard-desktop-bin
	
clean:
	@rm -f ./*.pkg.tar.zst
	@rm -f ./src/*.tar.gz
	@rm -f ./*.tar.gz
	@rm -rf ./pkg